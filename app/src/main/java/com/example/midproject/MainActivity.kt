package com.example.midproject

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private  var start = true
    private  var score = 0;
    private  var cda = 0;
    private  var againvar = true;
    private  var mogeba = 0;
    private  var wageba = 0;



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        again()
    }

    private fun init() {
        generate.setOnClickListener{
            generate()
        }
    }
    //Play again starts from
    private  fun again(){
        againButton.setOnClickListener{
            playAgain()
        }
    }



    private fun playAgain(){
        if(againvar){

            start = true;
            countTry.text = "გამოყენებული ცდები: 0";
            output.text = "";
            output1.text = "";
            output2.text = "";
            output3.text = "";
            mogeba = 0;
            wageba = 0;



        }
    }

    // ends

    //Generate numbers
    @SuppressLint("SetTextI18n")
    private  fun generate() {
        if (start) {
            val randomValues = List(4) { Random.nextInt(1, 6) }
            println(randomValues)
            cda++;
            output.text = randomValues[0].toString()
            output1.text = randomValues[1].toString()
            output2.text = randomValues[2].toString()
            output3.text = randomValues[3].toString()
            countTry.text = "გამოყენებული ცდები: $cda";


        }


    }
}




